#!/bin/bash

#== If webRoot has not been difined, we will set appRoot to webRoot
if [[ ! -n "$WEB_ROOT" ]]; then
  export WEB_ROOT=$APP_ROOT
fi

#== Composer install.
if [[ -f "$APP_ROOT/composer.json" ]]; then
  cd $APP_ROOT && composer install;
fi
#if [[ -f "$WEB_ROOT/composer.json" ]]; then
#  cd $WEB_ROOT && composer install;
#fi

#== Create settings files
cp $APP_ROOT/.devpanel/drupal9-settings.php $WEB_ROOT/sites/default/settings.php;

#== Config permission
cd $WEB_ROOT;
find sites themes modules profiles -type f -exec chmod g+w {} +;
find sites themes modules profiles -type d -exec chmod g+ws {} +;
chown -R 1000:82 $APP_ROOT/;